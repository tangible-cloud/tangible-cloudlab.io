---
title: 'Open call: Clearing up the mist'
date: 18th → 21st May 2022&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;21th → 24th June 2022
tags: Brussels

---
<div class="introduction">
In the frame of the research in art project "Clearing up the mist" supported by FrART, we are organizing two collective work sessions in <span class="town">Brussels</span>. First will take place from <span class="date">18th to 21st May 2022</span> and, the second from <span class="date">21th to 24th June 2022</span>.
</div>

> « In the present context, language that underpins cloud computing is instrumentally constructed by cloud providers, whom not only produce these technologies but also the metaphors appropriate for their business purposes. Given this commercial context, metaphors are mainly constructed for their business potential, not for their explanatory power. Cloud providers can, thus, strategically choose metaphors to their own advantage. They can use metaphors that direct human thought to certain images to avoid unwanted understandings. » <cite>— Lindh, Maria. « As a Utility – Metaphors of Information Technologies ». Human IT: Journal for Information Technology Studies as a Human Science 13, nᵒ 2 (13 mai 2016): 47‑80.</cite>

"Data-mining", "cloud computing", "computational power from the tap"... what views do these metaphors embed? Who produces this language? What forms of social, economic and political organization do they support? How do they affect our lives? And above all: how can we set up richer and more generous imaginaries and practices of the digital?


> # Metaphors we'd rather live by
> In their book "Metaphors We Live By" (1980), linguists George Lakoff and Mark Johnson say that metaphors are not simply a matter of language, but also of cognition. Omnipresent, unconscious and "the only means of perceiving and experiencing a large part of the world", they constitute our "ordinary conceptual system", thus conditions our perception of the world, our imagination, and the way we think and act. But by projecting the properties of a thing on another, we necessarily run up against the impossibility of establishing an equivalence. We can, at best, bring light to only one aspect.

<p class="split">
Because of the necessary abstraction of their complex functioning, computers and networks have always been domains particularly rich in metaphors, whether they are textual, visual or interactional. Based on the theories of Lakoff and Jonhson, this research project postulates that the invention of metaphors is an essential lever to get out of the current techno-political and economic paradigms and to propose other approaches to the digital world.
</p>
## Work sessions
Work sessions will propose an experimental and cross-disciplinary approach, in the form of a series of lectures and practical workshops. Starting from an analysis of the metaphors of cloud computing and of the ideologies they convey, we will work on the production of new "images" (visual, textual or interfaces), that would allow other aspects, visions and practices involved with these technologies to exist. The expected result, from speculative devices to prototypes, is meant to be a force of proposal.

## How to get involved?
The two sessions will take place in Brussels, from May 18 to 21 2022 and from June 21 to 24 2022 in the fabric lab Green Fabric:

Rue Jean-Baptiste Baeck 33,
1190 Forest — Brussels
Belgium

**If you wish to get involved, please send us an application to the following address: <a href="mailto:hello@tangible-cloud.be" class="email">hello@tangible-cloud.be</a>. before <span class="dead">Sunday, April 30, 2022</span>.**


**Please attach to your email:**

* a short presentation;
* the session(s) you want to attend;
* a note explaining why these questions matters to you

**The number of places is limited. We will give priority to candidates available for the full sessions.**

* We will confirm your participation within two weeks.
* Participation is free. The following expenses are covered
	* for all: lunch.
	* Please let us know if you would need an accommodation and financial support for travelling.
* Workshops will be held both in English and in French.

This call is subject to changes. The full program of each session will be communicated two weeks in advance.

<p class="split">
The works initiated during the sessions will be eligible for financial support, to pursue their development beyond the sessions. All of them will be part of a public restitution, at the end of 2022.
</p>
## Team
We are four graphic designers, developers or artists, from two collectives (Luuse and Open Source Publishing) brought together by a shared interest in digital tools, especially in the field of graphic design. Alongside our professional and artistic practice, we all teach in art schools, in different disciplines (digital art, typography, graphic design, web, printed image) and different places (ÉSA le 75, Erg, Ensba Lyon).

* Antoine Gelgon, designer, type design instructor at ERG. 
* Alex Leray, graphic designer, developer and teacher in digital image at ÉSA Le Septantecinq within the option Images Plurielles Imprimées (Brussels).
* Romain Marula, graphic designer, developer, artist and teacher at ESA Le Septantecinq.
* Marianne Plano, graphic designer and developer, teacher in digital design at Ensba Lyon.

Through our teaching experiences, we witnessed, in recent years, deep changes in the digital ecosystems in which schools are now embedded. These appears now to be leaning clearly towards the invisibilization of processes (disappearance of visible file systems in interfaces), the over determining of devices and software (shift towards tablets and their applications), and the rise of GAFAM (especially with Google Apps For Education). 

**This project is funded by FNRS-FRART (Fonds de la Recherche Scientifique), and supported by ÉSA Le Septantecinq.**

