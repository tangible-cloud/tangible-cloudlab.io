---
title: Exposition 
date: Du vendredi 3 mars au dimanche 5 mars  
infos:
 - 'Galerie KBK, Boulevard d’Ypres, 20 1000 Bruxelles, Belgique'
 - 'Vernissage : vendredi 3 mars 2023 à partir de 18 h' 
 - ' Performance à 19 h' 
tags: Ouverture de 10 h à 17 h les samedi 4 et dimanche 5 mars  

---


<div class="introduction">
Dans le cadre du projet Dissiper la brume, 30 personnes de différents horizons (artistes, designers, philosophes, économistes, etc.) se sont réunies en mai et juin 2022 pour imaginer, à travers des pratiques artistiques, des contre-récits à la vision dominante du numérique : le « cloud computing ».
</div>
<div class="cover" style="text-align: center">

<img src="/images/feu-bd.png" />

</div>

Ces rencontres ont donné lieu à une production diverse, à partir des propositions de **Élie Bolard, Seda Gürses et Femke Snelting (TITiPI), Dasha Ilina, et Marloes de Valk**: une série de cartes avec des mots exécutables aidant à réfléchir à des pratiques artistiques utilisant la technologie plus durables et solidaires ; des fables technologiques mettant en lumière les implications du cloud jusque dans notre intime, des vidéos parodiques questionnant notre relation paradoxale aux plateformes mêlant individualisme et isolement mais aussi désir de spiritualité et de communion ; ou bien des installations électroniques de serveurs dont l’exécution requiert des actions loufoques ou provocantes, pouvant présager d’un futur pas si lointain…

Cette exposition sera l’occasion de partager quelques unes des productions issues de cette recherche à travers **une performance de Stevie Ango et Clyde Lepage, une série de pièces issue des ateliers, l’édition d’un jeu de cartes, ainsi qu’un aperçu de la publication à venir et des 17 interviews réalisés pour l’occasion.**

> Avec la participation de Agnez Bewer, Anaelle Beignon, Davide Bevilacqua, Élie Bolard, Sofia Boschat Thorez, Simon Browne, Dennis de Bel, Marloes de Valk, Romain Cazier, Celo, Stéphane Degoutin, Denis Devos, Benjamin Gaulon, Antoine Gelgon, Seda Gürses, Chloé Horta, Brendan Howell, Dasha Ilina, Mathieu Lecouturier, Alexandre Leray, Lionel Maes, Nicolas Malevé, Romain Marula, Adrien Payet, Peggy Pierrot, Mariane Plano, Femke Snelting, Thomas Thibault, Tyler Reigeluth, Emma Sizun, Wendy Van Wynsberghe.

Un projet initié par Antoine Gelgon, Alexandre Leray, Marianne Plano et Romain Marula, porté par l’ÉSA Le 75 et réalisé avec le soutien du FRArt.

