---
title: 'Tangible Cloud'
template: 'poster.html'
---

# Tangible Cloud

## ››› [Open Call](/open-call) ‹‹‹
##### Apply!
### Clearing Up the Mist:<bR>Workshop Sessions

<div class="info">

<h4></h4>

<ol>
<li>18<sup>th</sup> → 21<sup>st</sup> May 22</li>

<li>21<sup>st</sup> → 24<sup>th</sup> June 22</li>

<li>Brussels</li>
</ol>
</div>

