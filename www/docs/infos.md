---
title: 'Second session practical informations'
date: 21th → 24st June 2022
tags: Brussels

---
<div class="introduction">
Dear participants,<br><br>

The second session of Clearing Up the Mist is coming soon, starting on <span class="date">Tuesday, the 21th of June</span> and ending on <span class="date">Friday, the 24th of June</span>. We are very happy to have gathered such a group of people and we hope those four days together will be fruitful.

This email summurizes important information about your arrival, the schedule and more. Please read it untill the end.
</div>

## Schedule and program

**We open the doors at 9.30am, and start at 10.00 on Tuesday the 21th of June**.
We "officially" finish at 5pm on Friday the 24st of June.

Most of the program will be held in English and French, though still with a possibility of interacting in English. Broadly speaking, we want to make sure that people are confortable with communication, and with various levels of fluency in English/French/Dutch/you name it… we welcome a nice mix of languages.

Please find a complete (yet to be adjusted at the margins) schedule here:  
<https://pads.domainepublic.net/p/r5wcc6mow6ntb9c5>

## Workspace

The session will take place at Green Fabric, a practionner-run space dedicated to textile creation, offering a shared fablab/workshop space, with a focus on social and environmental accountability. It is located in a quiet neighborhood, bordering the Jacques Brel parc and the oldest tree from Brussels (400 years old).

<https://greenfabric.be/>

Because it is a co-working space, there might be people working in the building. However, the space is large, modular and will be arranged as to make sure participants and co-workers don't disturb each others.

Here is the adress:

Rue Jean-Baptiste Baeck 33, 1190 Forest — Brussels Belgium
<https://www.openstreetmap.org/way/247791508>

To come from Midi station by public transport, 3 options:

* Tram 82, to Drogenbos kasteel/Chateau, stop Bempt (15-20 min) + 5 minutes walk;
* Bus 50, to Lot Station, stop Bempt (15-20 min) + 5 minutes walk;
* Train S1 to Nivelles/Nijvel, stop Uccle Stalle (5-7 min). Two trains per hour (departure hh:00 and hh:25 from track 21 in general). The workspace is just on the other side of the tracks, and there is a small secret passage to cross.

You can buy tickets at bus/tram stops, or on the bus/tram directly using a contactless credit card. Please keep all your tickets for refunding. Marianne will also be arriving at Midi on the first day (9.45), so you can contact her if anything.

## Accomodation

For those of you who requested an accomodation, you might already have received information, but we will make individual check-ups by e-mail. Locations are various but should, at top, be 15 minutes away from the workspace.

## Lunch and diet

Lunch will be served at the Green Fabric. If we are lucky enough to get some sunshine, we might be able to eat outside, near the workspace. 

The ground idea is to serve **a vegan lunch**, with food sourced from Nojavel, a neighbourhood anti-waste association, cooked by our friends Leo and Baptiste. <https://nojavel.org/>.

**If you have allergies or a more specific diet,  please let us know so they can offer an alternative.**

## Care

We will be a small team for four days. We assume that participants, including ourselves, will put care in maintening a fruitful and welcoming environment for everyone.

Code is no substitute for culture, and copy/pasting an existing code of conduct, without discussing it makes little sense. Because we don't have a lot of experience in organising such events, we will keep it short.

As a ground rule, **if anyone happens to feel bad about a situation (personnaly or involving others), it is important to raise awarness early, so we can isolate the incident and prevent further escalation**. The team is here to listen, and will do its best to find ways of easing and solving possible conflicts, together. You should also feel free to turn to someone outside the team, if sharing with that person is easier: many participants have been working on these matters, or have a lot of experience in setting up likely events, so they might also be able to help you.

## Documentation, tools

Marianne has prepared a Niceberry Pi loaded with:

* Etherpad
* An Apache Web server
* An FTP server
* A Git 
+  a few other goodies..!

These softwares, in addition to being available work tools for all participants, are meant to help us keep track of the projects that will be joinly conducted during these days. **If you have any suggestion or request for another tool not listed yet, please do let us know!** We'll be happy to provide it whenever possible.

And, of course, it might a very good idea to bring your laptop!

## Recording

For those of you making presentations, we would like to set up a recording device (audio only? video?), as a mean to build an archive of the event, and possibly publish theses on our website later, under a free licence. **If you feel uneasy with public broadcasting, please do let us know.** We could keep the records private, or simply not record at all. We are also happy to consider others forms of publication, such as written transcripts. **If you have prepared notes, slides or documentation, we'd be happy to discuss how to share them with you, as they might be relevant add-ons to a transcript.**

## Contacts

If anything, you can reach us at the phone numbers. We will communicate them seperatly.

Thanks a lot for reading that long page,

We're looking a lot forward to meeting all of you,

Warm regards,

The Tangible Cloud team

