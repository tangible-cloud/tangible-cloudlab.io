---
title: Exhibition 
date: Friday, March 3 to Sunday, March 5  
infos:
 - 'KBK Gallery, Boulevard d’Ypres, 20 1000 Brussels, Belgium'
 - 'Vernissage : Friday, March 3, 2023 starting at 6:00 pm.' 
 - 'Performance around 6:45 pm' 
tags: 'Open from 12 am. to 5 pm on Saturday&#44; March 4 and Sunday&#44; March 5' 

---


<div class="introduction">
As part of the project Dissipating the Mist, 30 people from different backgrounds (artists, designers, philosophers, economists, etc.) met in May and June 2022 to imagine, through artistic practices, counter-narratives to the dominant vision of digital : « cloud computing. "
</div>

<div class="cover" style="text-align: center">

<img src="/images/feu-bd.png" />

</div>
These encounters resulted in a diverse production, based on proposals by **Élie Bolard, Seda Gürses and Femke Snelting (TITiPI), Dasha Ilina, and Marloes de Valk**: a series of cards with executable words helping to reflect on more sustainable and supportive artistic practices using technology ; technological fables highlighting the implications of the cloud right into our intimacy ; parodic videos questioning our paradoxical relationship to platforms mixing individualism and isolation but also desire for spirituality and communion ; or electronic server installations whose execution requires zany or provocative actions, possibly foreshadowing a not so distant future…

This exhibition will be an opportunity to share some of the productions resulting from this research through **a performance by Stevie Ango and Clyde Lepage, a series of pieces from the workshops, the edition of a deck of cards, as well as a preview of the upcoming publication and the 17 interviews conducted for the occasion.

> With the participation of Agnez Bewer, Anaelle Beignon, Davide Bevilacqua, Élie Bolard, Sofia Boschat Thorez, Simon Browne, Dennis de Bel, Marloes de Valk, Romain Cazier, Celo, Stéphane Degoutin, Denis Devos, Benjamin Gaulon, Antoine Gelgon, Seda Gürses, Chloé Horta, Brendan Howell, Dasha Ilina, Mathieu Lecouturier, Alexandre Leray, Lionel Maes, Nicolas Malevé, Romain Marula, Adrien Payet, Peggy Pierrot, Mariane Plano, Femke Snelting, Thomas Thibault, Tyler Reigeluth and Emma Sizun.

A project initiated by Antoine Gelgon, Alexandre Leray, Marianne Plano and Romain Marula, supported by ÉSA Le 75 and realized with the support of FRArt.

Looking forward to meeting you,
