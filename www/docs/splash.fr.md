---
title: 'Tangible Cloud'
template: 'poster.html'
---
# Tangible Cloud

## ››› [Appel à candidatures](/fr/open-call) ‹‹‹
##### Apply!
### Dissiper la brume:<bR>Sessions de travail

<div class="info">

<h4></h4>

<ol>
<li>18 → 21 Mai 22</li>

<li>21 → 24 Juin 22</li>

<li>Bruxelles</li>
</ol>
</div>

