---
title: 🌦 Dissiper la brume 🌩 
date: Pratiques numériques par temps couvert 🌪
---
<div class="introduction">
Dans le cadre du projet Dissiper la brume, 30 personnes de différents horizons (artistes, designers, philosophes, économistes, etc.) se sont réunies en mai et juin 2022 pour imaginer, à travers des pratiques artistiques, des contre-récits à la vision dominante du numérique : le « cloud computing ».
</div>

Ces rencontres ont donné lieu à une production diverse, à partir des propositions de **Élie Bolard, Seda Gürses et Femke Snelting (TITiPI), Dasha Ilina, et Marloes de Valk**: une série de cartes avec des mots exécutables aidant à réfléchir à des pratiques artistiques utilisant la technologie plus durables et solidaires ; des fables technologiques mettant en lumière les implications du cloud jusque dans notre intime, des vidéos parodiques questionnant notre relation paradoxale aux plateformes mêlant individualisme et isolement mais aussi désir de spiritualité et de communion ; ou bien des installations électroniques de serveurs dont l’exécution requiert des actions loufoques ou provocantes, pouvant présager d’un futur pas si lointain…

Cette exposition a été l'occasion de partager une partie des productions réalisé lors des worksessions, ainsi qu'un avant-goût d'une publication à venir comprenant dix-sept entretiens réalisés avec les participants. Elle présenta également une performance de Stevie Ango et Clyde Lepage, une série de pièces issue des ateliers, l’édition d’un jeu de cartes, ainsi qu’un aperçu de la publication à venir et des 17 interviews réalisés pour l’occasion.

> Ce projet a vu la participation de Agnez Bewer, Anaelle Beignon, Davide Bevilacqua, Élie Bolard, Sofia Boschat Thorez, Simon Browne, Dennis de Bel, Marloes de Valk, Romain Cazier, Celo, Stéphane Degoutin, Denis Devos, Benjamin Gaulon, Antoine Gelgon, Seda Gürses, Chloé Horta, Brendan Howell, Dasha Ilina, Mathieu Lecouturier, Alexandre Leray, Lionel Maes, Nicolas Malevé, Romain Marula, Adrien Payet, Peggy Pierrot, Mariane Plano, Femke Snelting, Thomas Thibault, Tyler Reigeluth, Emma Sizun, Wendy Van Wynsberghe.

Un projet initié en 2021 par Antoine Gelgon, Alexandre Leray, Marianne Plano et Romain Marula, porté par l’ÉSA Le 75 et réalisé avec le soutien du FRArt.

<a href="https://docs.tangible-cloud.be/presentation-open_open" target="_blank">Slide de présentation à Open-Open </a> |
<a href="https://docs.tangible-cloud.be/publications-pdf" target="_blank">Entretiens en pdf</a> |
<a href="https://archives.tangible-cloud.be" target="_blank">Archives worksessions</a> |