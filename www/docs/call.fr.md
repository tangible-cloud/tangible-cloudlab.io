---
title: 'Appel à participation: Dissiper la brume' 
date: 18 → 21 mai 2022&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;21 → 24 juin 2022
tags: Bruxelles

---
<div class="introduction">
Dans le cadre du projet de recherche en art «Dissiper la brume» soutenu par le FrART, nous organisons à <span class="town">Bruxelles</span> deux sessions de travail collectif du <span class="date">18 au 21 mai 2022</span> et <span class="date">21 au 24 juin 2022</span>.
</div>

> « In the present context, language that underpins cloud computing is instrumentally constructed by cloud providers, whom not only produce these technologies but also the metaphors appropriate for their business purposes. Given this commercial context, metaphors are mainly constructed for their business potential, not for their explanatory power. Cloud providers can, thus, strategically choose metaphors to their own advantage. They can use metaphors that direct human thought to certain images to avoid unwanted understandings. » <cite>— Lindh, Maria. « As a Utility – Metaphors of Information Technologies ». Human IT: Journal for Information Technology Studies as a Human Science 13, nᵒ 2 (13 mai 2016): 47‑80.</cite>

Du fait de sa complexité, c'est par le biais de métaphores, d'images, d'abstractions, que nous appréhendons l'informatique. Les métaphores ne pouvant mettre en lumière certains aspects qu'au détriment d'autres, leur choix conditionnent notre perception et nos pratiques du numérique. Alors que l'informatique et les réseaux ont pris une place centrale dans nos vies et que notre expérience du numérique se réduit de plus en plus à l'utilisation de services verrouillés distants, quelles métaphores structurent désormais notre imaginaire numérique?


«Data-mining», «cloud computing», «computational power from the tap»… quelles visions du monde ces métaphores véhiculent-elles? Qui produit ce langage? Quelles formes d'organisations sociale, économique et politique soutiennent-elles? Comment nos vies en sont-elles affectées? Et surtout: comment pouvons-nous produire d'autres imaginaires et pratiques du numérique plus riches et généreuses?

<span class="break"></span>

> # Metaphors we'd rather live by
> Dans leur ouvrage «Metaphors We Live By» (1980), les linguistes George Lakoff et Mark Johnson postulent que les métaphores ne relevent pas simplement du domaine du langage, mais aussi du domaine cognitif. Omniprésentes, inconscientes et «seuls moyens de percevoir et d'expérimenter une grande partie du monde», elles constituent notre «système conceptuel ordinaire», conditionnant ainsi notre perception du monde, notre imaginaire, notre manière de penser et d'agir. Mais en projetant les propriétés d'une chose sur une autre, nous nous heurtons nécessairement à l'impossibilité d'établir une équivalence. Nous ne pouvons, au mieux, n'en éclairer qu'un aspect.
		
Par la nécéssaire abstraction de leur fonctionnement complexe, l'informatique et les réseaux sont depuis toujours des domaines particulièrement riches en métaphores, qu'elles soient textuelles, visuelles ou d'interaction. En se basant sur les théories le Lakoff et Jonhson, ce projet de recherche postule que l'invention de métaphores est un levier essentiel pour s'extraire des paradigmes techno-politiques et économiques actuels et proposer d'autres approches du numérique.

## Sessions de travail
Les sessions de travail proposeront une approche exploratoire et interdisciplinaires, sous la forme d'une série de conférences et d’ateliers pratiques. À partir d'une critique des métaphores du «cloud computing» et des idéologies qu'elles véhiculent, nous travaillerons à la production de nouvelles «images» (visuelles, textuelles ou d'interfaces), permettant de faire émerger d'autres aspects et d'autres visions de ces technologies. Le résultat escompté, allant du dispositif spéculatif au prototype, aura valeur de force propositionnelle.

## Comment participer?
Les deux sessions auront lieu à Bruxelles, du 18 au 21 mai 2022 et du 21 au 24 juin 2022 au sein de du textile lab Green Fabric [33 rue jean-baptiste Baeck, 1190 Forest]

Si vous souhaitez participer, envoyez-nous votre candidature à l'adresse suivante : <a href="mailto:hello@tangible-cloud.be" class="email">hello@tangible-cloud.be</a>. Date limite: <span class="dead">dimanche 30 avril 2022</span>.

**Merci de joindre à votre email:**  

* une courte présentation;   
* la ou les sessions auxquelles vous voulez participer;
* un mot sur pourquoi ce sujet vous intéresse.  


<span class="break"></span>

<p class="split">
Le nombre de places est limité. Nous donnerons la priorité aux personnes disponibles pour les deux sessions.
</p>

* nous vous confirmerons votre participation sous deux semaines.
* la participation est gratuite. Les dépenses suivantes sont couvertes:
	* pour tous : déjeuner.
	* merci de nous préciser dans le mail si vous avez d'un besoin d'un logement et d'un support financier pour voyager.
* La communication se fera en anglais et français.

Cet appel est susceptible d'être mis à jour. Le programme complet de chaque session sera communiqué deux semaines à l'avance.

Les productions amorcées durant les sessions pourront bénéficier d'une aide financière pour prolonger leur developpement au delà des ateliers, et prendre place dans une restitution publique, fin 2022.

## Équipe
Nous sommes quatre designeurs graphiques, développeurs ou artistes, issus de deux collectifs (Luuse et Open Source Publishing) rassemblés par un intérêt commun pour le rôle des outils numériques dans le champ de la création graphique. En parallèle de notre pratique professionnelle et artistique, nous sommes enseignants en école d’art, dans différentes disciplines (art numérique, typographie, graphisme, web, image imprimée) et différents lieux (ÉSA le 75, Erg, Ensba Lyon).

* Antoine Gelgon, designer, intervenant en dessin de caractère à l'ERG.
* Alex Leray, graphiste, developpeur et enseignant en image numérique à l'ÉSA Le Septantecinq au sein de l'option Images Plurielles Imprimées (Bruxelles).
* Romain Marula, graphiste, developpeur, artiste et enseignant à l'ESA Le Septantecinq.
* Marianne Plano, graphiste et développeuse, enseignante en design numérique à l'Ensba Lyon.

À travers nos expériences d'enseignement en école d'art, nous avons observé ces dernières années de profonds changements dans les écosystèmes numériques pédagogiques, avec une nette tendance à l'invisibilisation des processus (par exemple, via la disparition des systèmes hiérarchiques de navigation des gestionnaires de fichiers), la surdétermination de appareils et logiciels (par exemple, via l'arrivé des tablettes et de leurs applications) et la montée en puissance des GAFAM (notamment Google Apps For Education).

